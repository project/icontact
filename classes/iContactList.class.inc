<?php

/**
 * @file
 * Defines an iContact List class.
 */

class iContactList extends iContactResource {
  protected $type = 'list';

  public $accountId;
  public $clientFolderId;
  public $listId;
  public $name = '';
  public $emailOwnerOnChange = 1;
  public $welcomeOnManualAdd = 0;
  public $welcomeOnSignupAdd = 0;
  public $welcomeMessageId = 0;
  public $description = '';

  protected function init($accountId, $clientFolderId, $listId = NULL) {
    $this->accountId = $accountId;
    $this->clientFolderId = $clientFolderId;
    if ($listId) {
      $this->listId = $listId;
      $this->getData();
    }
  }

  protected function cacheId() {
    return $this->type .':'. $this->accountId .':'. $this->clientFolderId .':'. $this->listId;
  }

  protected function setCache() {
    if ($this->listId) {
      parent::setCache();
    }
  }

  protected function getURI() {
    return '/a/'. $this->accountId .'/c/'. $this->clientFolderId .'/lists/'. $this->listId;
  }

  public function getAccount() {
    $account = &$this->iContact->getAccount($this->accountId);
    return $account;
  }

  public function getClientFolder() {
    $clientfolder = &$this->getAccount()->getClientFolder($this->clientFolderId);
    return $clientfolder;
  }

  public function getContacts() {
    $contacts = array();
    $clientfolder = &$this->getClientFolder();
    foreach ($clientfolder->requestContacts() as $contactId) {
      if ($clientfolder->getSubscription($this->listId . '_' . $contactId)) {
        $contacts[$contactId] = $contactId;
      }
    }
    return $contacts;
  }

  public function subscribe($fields, $messages = array()) {
    $clientfolder = $this->getClientFolder();
    $contact = &$clientfolder->getContact(array('email' => $fields['email']));
    if (!$contact) {
      // Contact doesn't exists, add new contact.
      $contact = &$clientfolder->initContact();
      try {
        $contact->create($fields);
        $contact->subscribe($this->listId);
        if (!empty($messages['create'])) {
          drupal_set_message(check_plain($messages['create']));
        }
        return;
      }
      catch (Exception $e) {
        if (!empty($messages['error'])) {
          drupal_set_message(check_plain($messages['error']), 'error');
        }
        watchdog('iContact', $e->getMessage(), array(), WATCHDOG_ERROR);
        return;
      }
    }
    else {
      // Contact already exists, update information if needed.
      try {
        $data = array_filter($fields);
        foreach ($data as $key => $field) {
          if ($key != 'email' && $contact->$key == $field) {
            unset($data[$key]);
          }
        }
        if (!empty($data)) {
          $contact->update($data);
        }

        $contact->subscribe($this->listId);
        if (!empty($messages['update'])) {
          drupal_set_message(check_plain($messages['update']));
        }
        return;
      }
      catch (Exception $e) {
        if (!empty($messages['error'])) {
          drupal_set_message(check_plain($messages['error']), 'error');
        }
        watchdog('iContact', $e->getMessage(), array(), WATCHDOG_ERROR);
        return;
      }
    }
  }
}
